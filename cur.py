#!/usr/bin/env python3
import numpy as np
from numpy.linalg import norm
from sklearn.preprocessing import StandardScaler, MaxAbsScaler, RobustScaler
from scipy.stats.stats import pearsonr

def CUR(fm, ncols=20, skip_tol=False, nlog=4, k=3, tol=1e-2, row_sel=False):
    """CUR approximation for auto-selection of fingerprints and
    reference structures (training images). Here we only deal
    with candidates using Gaussian descriptors. The algorithm
    applies to other descriptors as well.

    Parameters
    ----------
    fm: numpy.ndarray
    Feature matrix in the format of `numpy array`. The column indicates
    the feature and the row indicates the point (image). Note that as of
    now the feature vectors for atoms in the model system are combined
    horizontally.

    ncols: int
    Number of columns to be selected. It could be specified. Or if not 
    supplied it can be found automatically when the F-norm relative error 
    between the original feature matrix and the approximation is below 
    the threshold as defined by the parameter `tol`

    nlog: int (default :4)
    If ncols is None, the number of columns to start with is given by
    log10(sigma[0]/sigma[ncols]) > nlog  

    k: int or str(default: 3)
    number of components for evaluation of the column importance score.
    If k is a `str`, it will be chosen as the minimum integer which satisfies
    log10(sigma[0]/sigma[k]) > 2.
    
    tol: float (default: 1e-4)
    Stopping threshold for searching of columns

    row_sel: bool (default: False)
    if True, row selection (ref. structures ) will be implemented rather than
    column selection (features). 

    Return
    ---------
    indices of columns selected. *Note* that it should be used along
    with the list of descriptors.
    """
    skip_tol = True if isinstance(ncols, int) else False
    if row_sel:
        X = fm.copy().T
    else:
        X = fm.copy()

    indices = -np.ones(ncols)
    X = np.array(X)
    nf = X.shape[1]
    masked = (np.var(X, axis=0) < 1e-3) # mask almost constant features
    unmasked = ~masked
    # print(masked.sum(), unmasked.sum())
    reduced_candidates = np.arange(nf)[unmasked]
    X[:, reduced_candidates] = RobustScaler().fit_transform(X[:, reduced_candidates])
    for i in range(ncols):
        X, index, reduced_candidates = col_sel(X, k, reduced_candidates)
        indices[i] = index
    dev = get_dev(X, indices)
    print(f"Relative norm deviation is: {dev*100:.3f}%")
    indices = indices.astype(int).tolist()
    return indices

    """The following code is for automatic selection
       of the number of fingerprints. However it is
        not suggested since computing deviation is
        not cheap"""
    # while True:
    #     if skip_tol:
    #         indices = indices.astype(int)
    #         dev = get_dev(fm, indices)
    #         return indices, dev
    #     if dev < tol:
    #         indices = indices.astype(int)
    #         return indices, dev
    #     else:
    #         X, ind = col_sel(X, k)
    #         indices = np.concatenate([indices, [ind]])
    #         dev = get_dev(fm, indices)

def col_sel(X, k, candidates, tol=1e-3):
    """Select single column from feature matrix (X)"""
    fm = X[:, candidates]
    u, sigma, vh = np.linalg.svd(fm)
    prob_cols = np.square(vh)[0:k,:].sum(axis=0)
    index = np.argmax(prob_cols)
    selected_index = candidates[index]
    selected = X[:, selected_index]
    # del_list = []
    # for _, m in enumerate(candidates):
    #     if abs(pearsonr(X[:, m], X[:, selected_index])[0]) > 0.95:
    #         del_list.append(_)
    # print(len(del_list))
    # candidates = np.delete(candidates, del_list)
    candidates = np.delete(candidates, index)
    for j in candidates:
        X[:,j] = X[:,j] - selected*(np.dot(selected, X[:,j]))/norm(selected)**2
    return X, selected_index, candidates

def get_dev(fm, indices):
    """Compute the F-norm relative deviation using selected columns"""
    indices = indices.astype(int)
    # Compute the F-norm relative approximation error
    C = fm[:, indices]
    R = fm
    # compute the pseudo-inverse
    C_plus = np.linalg.pinv(C)
    R_plus = np.linalg.pinv(R)
    U = np.linalg.multi_dot([C_plus, fm, R_plus])
    fm_hat = np.linalg.multi_dot([C, U, R])
    # print(norm(fm_hat-fm), norm(fm))
    dev = norm(fm_hat-fm)/norm(fm)
    return dev