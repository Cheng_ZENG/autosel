#!/usr/bin/env python3
import numpy as np
from numpy.linalg import norm
from scipy.stats.stats import pearsonr
import pandas as pd
from os.path import isdir
from amp.utilities import hash_images
from collections import Counter
import pickle 
from sklearn.preprocessing import MinMaxScaler, RobustScaler, normalize

def get_atomic_force(images, descriptor="G", save_pickle=True):
    """
    Compute the atomic forces for all images

    Parameters
    ------------
    images : list or str
    List of ASE atoms objects with positions, symbols, energies, and
    forces in ASE format. This is the training set of data. This can
    also be the path to an ASE trajectory (.traj) or database (.db)
    file.

    descriptor: str
    'G' or 'Z'

    save_pickle: bool
    save results to a pickle file or not

    Return
    -------
    correlation matrix between force and feature matrices.
    """
    hashed_images = hash_images(images, ordered=True)
    atoms = list(hashed_images.values())
    n_img = len(hashed_images.keys())
    n_atoms = len(atoms[0])
    symbols = atoms[0].get_chemical_symbols()
    elements = list(set(symbols))
    counter = Counter(symbols)
    f = {}
    for element in elements:
        # generate nan matrices for each element
        f[element] = np.empty([n_img*n_atoms, 1])
        f[element][:] = np.nan
    for i, hash_id in enumerate(hashed_images.keys()):
        atoms_per = atoms[i]
        forces = atoms_per.get_forces(apply_constraint=False)
        for j, symbol in enumerate(symbols):
            force = forces[j]
            f[symbol][n_atoms*i+j, 0] = np.linalg.norm(force)
    for element in elements:
        # print(fps[element].shape[0])
        f[element] = f[element][~np.isnan(f[element]).any(axis=1)]
        # print(fps[element].shape[0], counter[element]*n_img)
        assert f[element].shape[0] == counter[element]*n_img
    if save_pickle:
        with open("forces_%s.pkl" % descriptor, "wb") as pf:
            pickle.dump(f, pf)  
    return f

def col_sel_by_corr(fm, forces, ncols):
    """
    Select the features based on their absoluate correlation values
    with the atomic force magnitude.
    
    Parameters
    ------------
    fm: list or numpy.ndarray
    feature matrices

    forces: list or numpy.ndarray
    2D force matrix

    ncols: int
    number of columns to be selected

    Return
    ------
    correlation matrix sorted by absolute values of correlation to
    the atomic force magnitude
    """
    fm_c = np.array(fm)
    nf = fm_c.shape[1]
    forces = np.array(forces).reshape(-1)
    indices = np.zeros(ncols)
    masked = (np.var(fm_c, axis=0) < 1e-3) # mask almost constant features
    unmasked = ~masked
    reduced_candidates = np.arange(nf)[unmasked]
    fm_c[:, reduced_candidates] = RobustScaler().fit_transform(fm_c[:, reduced_candidates])
    # fm_c[:, reduced_candidates] = normalize(fm_c[:, reduced_candidates], axis=0)
    forces /= norm(forces)
    for i in range(ncols):
        corrs = np.zeros(len(reduced_candidates))
        for _, k in enumerate(reduced_candidates):
            corrs[_] = pearsonr(fm_c[:, k], forces)[0]
        index = np.argmax(abs(corrs))
        # print(max(abs(corrs)), min(abs(corrs)))
        index_selected = reduced_candidates[index]
        indices[i] = index_selected
        # pop out the selected feature from the candidates
        # also, eliminate features highly correlated to the selected one
        del_list = []
        for _, m in enumerate(reduced_candidates):
            if abs(pearsonr(fm_c[:, m], fm_c[:, index_selected])[0]) > 0.95:
                del_list.append(_)
        # print(len(del_list))
        reduced_candidates = np.delete(reduced_candidates, del_list)
        selected = fm_c[:, index_selected]
        # Orthogonalization
        for j in reduced_candidates:
            fm_c[:,j] = fm_c[:,j] - selected*(np.dot(selected,fm_c[:,j]))\
                /norm(selected)**2
    indices = indices.astype(int)
    return indices