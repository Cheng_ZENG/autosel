#!/usr/bin/env python3
import pickle
import numpy as np
from os.path import isdir
from ase.io import read
from collections import Counter
from scipy.spatial import distance_matrix
from amp.utilities import hash_images, assign_cores, Logger
from amp.descriptor.cutoffs import Cosine, Polynomial
from amp.descriptor.gaussian import make_symmetry_functions
from amp.descriptor.gaussian import Gaussian
# from amp.descriptor.zernike import Zernike


def make_descriptors(images, method, save_G=True, forces=False, label='amp-G',
                cutoff={'scheme':'cosine', 'Rc':6.5}, fortran=True):
    """
    It aims to generate feature vectors given the trajectory. 
    Here `Gaussian` or `Zernike` descriptors are implemented.  

    Parameters
    ------------
    images : list or str
    List of ASE atoms objects with positions, symbols, energies, and
    forces in ASE format. This is the training set of data. This can
    also be the path to an ASE trajectory (.traj) or database (.db)
    file. 

    method: dict
    In the `method` dict, it should contain two keys, decriptor
    and param_grid 
    - 'decriptor': str.  "gaussian" or 'zernike'.
    - 'param_grid': dict of dict. Parameters to be looped over.

    cutoff: dict
    'scheme': `cosine` or `polynomial`; 'Rc': cutoff distance to which
    the neighbors are calculated.

    Return
    --------
    list of all descriptors.
    """
    hashed_images = hash_images(images, ordered=True)
    # print(hashed_images.values())
    elements = sorted(list(set(list(hashed_images.values())[0].get_chemical_symbols())))
    cutoff_scheme = cutoff['scheme']
    Rc = cutoff['Rc']
    assert cutoff_scheme in ['cosine', 'polynomial']
    if cutoff_scheme == 'cosine':
        cutoff_func = Cosine(Rc=Rc)
    elif cutoff_scheme == 'polynomial':
        cutoff_func = Polynomial(Rc=Rc, gamma=4)
    descriptor = method['descriptor']
    param_grid = method['param_grid']
    assert descriptor in ['gaussian', 'zernike']
    if descriptor == "gaussian":
        assert set(param_grid.keys()).issubset(set(['G2', 'G4', 'G5']))
        G = []
        for G_type, params in param_grid.items():
        # for G_type, params in param_grid.iteritems(): # python 2
            if G_type == 'G2':
                if 'offsets' in params.keys():
                    G += make_symmetry_functions(elements=elements,
                        type=G_type, etas=params['eta'], offsets=params['offsets'])
                else:
                    G += make_symmetry_functions(elements=elements,
                    type=G_type, etas=params['eta'])
            else:
                G += make_symmetry_functions(elements=elements,
                    type=G_type, etas=params['eta'], zetas=params['zeta'],
                    gammas=params['gamma'])
        Gs = {}
        for element in elements:
            Gs[element] = G
        gaussian = Gaussian(Gs=Gs, cutoff=cutoff_func, dblabel=label, fortran=fortran)
        gaussian.calculate_fingerprints(hashed_images, calculate_derivatives=forces,
                                        log=Logger('fingerprinting.log'))
        if save_G:
            with open("G.pkl", "wb") as pf:
                pickle.dump(G, pf)
        return G
    if descriptor == 'zernike':
        assert 'nmax' in param_grid.keys()
        assert cutoff_scheme  == "cosine"
        if 'Gs' not in param_grid.keys():
            Gs = None
        else:
            Gs = param_grid['Gs']
        nmax = int(param_grid['nmax'])
        zernike = Zernike(cutoff=cutoff_func, nmax=nmax, Gs=Gs,
                           dblabel="amp-Z", fortran=fortran)
        zernike.calculate_fingerprints(hashed_images)
        nf = nmax + (nmax / 2)**2 + 1 if nmax % 2 == 0 else \
             nmax + (nmax**2 - 1) / 4 + 1
        return nf


def make_feature_matrices(images, label='G', forces=False,
                          save_pickle=True):
    """Make element-wise feature matrices.

    Parameter
    ---------
    images: 

    label: str
    label for fingerprints and fingerprint

    save_pickle: bool
    Save the feature matrice dictionary or not

    Return
    --------
    A dictionary of feature matrices. 
    For the dictionary, "keys" are elements of the model system. "Values" are
    corresponding feature matrices. For atoms of the same type elements,
    their fingerprints are concatenated vertically since it is mainly
    designed for feature selection.
    """
    fps_folder = "amp-%s-fingerprints.ampdb/loose/" % label
    if forces:
        dfps_folder = "amp-%s-fingerprint-primes.ampdb/loose/" % label
        if not isdir(fps_folder) or not isdir(dfps_folder):
            raise RuntimeError('Calculate fingerprints and fingerprint derivatives'
                               ' first')
    else:
        if not isdir(fps_folder):
            raise RuntimeError('Calculate fingerprints first')
    reduced_indices =  _get_atomic_indices_of_concern(images)
    hashed_images = hash_images(images, ordered=True)
    atoms = list(hashed_images.values())[0]
    symbols = atoms.get_chemical_symbols()
    elements = list(set(symbols))
    fps, dfps = {}, {}
    for element in elements:
        fps[element], dfps[element] = [], []
    for i, hash_id in enumerate(hashed_images.keys()):
        file = fps_folder + hash_id
        with open(file, 'rb') as pf:
            fp = pickle.load(pf)
        for i, _ in enumerate(fp):
            # if i in reduced_indices:
            fps[_[0]].append(_[1])
        if forces:
            dfp_file = dfps_folder + hash_id
            with open(dfp_file, 'rb') as pf:
                fp_primes = pickle.load(pf)
            for _, fp_prime in fp_primes.items():
                # only consider z direction derivative and
                # surface metal atoms that matter
                if _[-1]  == 2 and _[0] in reduced_indices:
                    ele = _[1]
                    dfps[ele].append(fp_prime)
    if save_pickle:
        with open("fv_train_%s.pkl" % label, "wb") as pf:
            pickle.dump(fps, pf)
        if forces:
            with open("dfv_train_%s.pkl" % label, "wb") as pf:
                pickle.dump(dfps, pf)
    return fps, dfps

def _get_atomic_indices_of_concern(traj, cutoff=6.5, ):
    atoms = read(traj, index=':')
    dm_tensor = np.zeros([len(atoms), len(atoms[0]), len(atoms[0])])
    for i, _ in enumerate(atoms):
        positions = _.positions
        dm_tensor[i,] = cosine_cutoff_fxn(distance_matrix(positions, positions),
                                          Rc=cutoff)
    # Variance of in-cell inter-atomic distances
    # More reasonable to use MIC distances?
    dm_var = np.var(dm_tensor, axis=0)
    shape = (len(atoms[0]), len(atoms[0]))
    top_combos = np.argsort(dm_var, axis=None)[-len(atoms[0]):]

    indices = np.array(list(set([v for _ in top_combos for v in
                        np.unravel_index(_, shape) ])))
    # indices.astype(int)
    return indices

def cosine_cutoff_fxn(Rij, Rc=6.5):
    return 0.5 * (np.cos(np.pi * Rij / Rc) + 1.) * (Rij < Rc)